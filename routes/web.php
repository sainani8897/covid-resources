<?php

use App\Http\Controllers\Admin\AddPermissionsController;
use App\Http\Controllers\Admin\AddPlasmaDonerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('pages.dashboard');
});

Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'Admin\DashboardController@index')->name('home');

//Plasma
Route::get('plasma-verify/{plasma}', 'Admin\AddPlasmaDonerController@verify')->name('plasma.verify');
Route::resource('plasma', 'Admin\AddPlasmaDonerController');

Route::get('blood-verify/{bloodDonar}', 'Admin\AddBloodController@verify')->name('blood.verify');
Route::resource('blood', 'Admin\AddBloodController');

Route::get('ambulance-services-verify/{ambulance_service}', 'Admin\AddAmbulanceServicesController@verify')->name('ambulance-services.verify');
Route::resource('ambulance-services', 'Admin\AddAmbulanceServicesController');

//Har Oxygen
Route::get('oxygen-verify/{oxygen}', 'Admin\AddOxygenController@verify')->name('oxygen.verify');
Route::resource('oxygen', 'Admin\AddOxygenController');

Route::middleware(['auth','role:super-admin'])->group(function () {
    Route::resource('permissions', 'Admin\AddPermissionsController');
    Route::resource('roles', 'Admin\AddRolesController');
});

Route::middleware(['auth'])->group(function () {
    Route::get('profile', 'Admin\UserController@profile')->name('profile');
    Route::get('edit-profile', 'Admin\UserController@editProfile')->name('edit.profile');
    Route::post('edit-profile', 'Admin\UserController@update')->name('update.profile');
});

// Requests
Route::resource('requests', 'Admin\RequestsController');



