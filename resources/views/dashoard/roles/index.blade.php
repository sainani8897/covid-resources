@extends('layouts.main')

@section('title', 'Roles')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="float-sm-end pb-4">
            <a href="{{route('roles.create')}}"  class="btn btn-success">Add Roles <span class="btn-inner--icon"><i class="ni ni-plus-61"></i></span></a>
        </div>
        <div class="clearfix"></div>
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Roles</h6>
          </div>
          <div class="card-body px-0 pt-0 pb-2">

            @livewire('dashboards.roles')

          </div>
        </div>
      </div>
</div>
@endsection
