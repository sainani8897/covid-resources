@extends('layouts.main')

@section('title', 'Roles')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Permission</h6>
          </div>
          <div class="card-body px-4 pt-2 pb-2">
            <form action="{{route('roles.update',$role->id)}}" method="POST">
                @method('PUT')
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Role</label>
                      <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{$role->name}}" name="name" placeholder="Permission">
                      @error('name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastt_name">Guard</label>
                      {{-- <input type="text" placeholder="Your Guard Name" class="form-control @error('guard_name') is-invalid @enderror" id="guard_name" name="guard_name"/> --}}
                      <select class="form-control @error('guard_name') is-invalid @enderror" name="guard_name" id="exampleFormControlSelect1">
                        <option value="">Select a Guard</option>
                        <option {{$role->guard_name=='web'?"selected":''}}>web</option>
                        <option {{$role->guard_name=='api'?"selected":''}}>api</option>
                    </select>
                      @error('guard_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                {{-- {{dd($role->permission_ids)}} --}}

                <div class="row">
                  <div class="col-md-10">
                    <label for="lastt_name">Permissions </label>
                    @foreach ($premissions as $premission)
                      <div class="form-check form-check">
                        <input {{in_array($premission->id,$role->permission_ids)?"checked":""}} class="form-check-input" type="checkbox" name="permission[]" id="inlineCheckbox1" value="{{$premission->name}}">
                        <label class="form-check-label" for="inlineCheckbox1">{{$premission->name}}</label>
                      </div>
                    @endforeach
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center">
                        @csrf
                        <a href="{{route('roles.index')}}" class="btn btn-outline-secondary w-10 mt-4 mb-5 px-3">Go Back</a>
                        <button type="submit" class="btn bg-gradient-info w-20 mt-4 mb-5">Submit</button>
                    </div>
                  </div>
                </div>
                {{-- <div class="row"> --}}
            </form>
          </div>
        </div>
      </div>
</div>

@endsection
