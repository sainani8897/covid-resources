@extends('layouts.main')

@section('title', 'Permissions')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Donor Information</h6>
          </div>
          <div class="card-body px-4 pt-2 pb-2">
            <form action="{{route('blood.update',$blood->id)}}" method="POST">
                @method('PUT')
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                      <input type="text" class="form-control @error('first_name') is-invalid @enderror"  value="{{$blood->first_name}}" name="first_name" placeholder="Your First Name">
                      @error('first_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastt_name">Last Name</label>
                      <input type="text" placeholder="Your Last Name" class="form-control @error('last_name') is-invalid @enderror" id="last_name" value="{{$blood->last_name}}" name="last_name"/>
                      @error('last_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="example-date-input" class="form-control-label">Date of Bith (D.O.B)</label>
                        <input class="form-control @error('bith_date') is-invalid @enderror" type="date" value="{{date('Y-m-d',strtotime($blood->bith_date))}}" name="bith_date"  id="bith_date">
                        @error('bith_date')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Age</label>
                      <input type="text" placeholder="Age" class="form-control" id="age" disabled/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Gender</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$blood->gender=="male"?'checked':""}} name="gender" value="male">
                            <label class="custom-control-label" for="customRadio1">Male</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$blood->gender=="female"?'checked':""}} name="gender" value="female">
                            <label class="custom-control-label" for="customRadio1">Female</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$blood->gender=="other"?'checked':""}} name="gender" value="other">
                            <label class="custom-control-label" for="customRadio1">Other</label>
                        </div>
                        @error('gender')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Blood Group</label>
                        <select class="form-control @error('blood_group') is-invalid @enderror" value="{{$blood->id}}" name="blood_group" id="exampleFormControlSelect1">
                            <option value="">Select a Blood Group</option>
                            <option {{$blood->blood_group=="O+"?'selected':""}} >O+</option>
                            <option {{$blood->blood_group=="O-"?'selected':""}} >O-</option>
                            <option {{$blood->blood_group=="A+"?'selected':""}} >A+</option>
                            <option {{$blood->blood_group=="A-"?'selected':""}} >A-</option>
                            <option {{$blood->blood_group=="B-"?'selected':""}} >B-</option>
                            <option {{$blood->blood_group=="B+"?'selected':""}} >B+</option>
                            <option {{$blood->blood_group=="AB+"?'selected':""}} >AB+</option>
                            <option {{$blood->blood_group=="AB-"?'selected':""}} >AB-</option>
                        </select>
                        @error('blood_group')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastt_name">Have you Donted blood earlier?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$blood->blood_donation_histroy=="yes"?'checked':""}} name="blood_donation_histroy" value="yes">
                                <label class="custom-control-label" for="customRadio1">Yes</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$blood->blood_donation_histroy=="no"?'checked':""}} name="blood_donation_histroy" value="no">
                                <label class="custom-control-label" for="customRadio1">No</label>
                            </div>
                            @error('blood_donation_histroy')
                            <small class="alert-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastt_name">Do you currently have symptoms?</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$blood->current_symptoms=="yes"?'checked':""}}  name="current_symptoms" value="yes">
                                <label class="custom-control-label" for="customRadio1">Yes</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$blood->current_symptoms=="no"?'checked':""}}  name="current_symptoms" value="no">
                                <label class="custom-control-label" for="customRadio1">No</label>
                            </div>
                            @error('current_symptoms')
                            <small class="alert-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="lastt_name">Have you ever had any of the following risk factors for HIV/AIDS, STDs or Hepatitis?</label>
                          <div class="form-check">
                              <input class="form-check-input" type="radio" {{$blood->hiv_hep_std=="no"?'checked':""}} name="hiv_hep_std" value="yes">
                              <label class="custom-control-label" for="customRadio1">Yes</label>
                          </div>
                          <div class="form-check">
                              <input class="form-check-input" type="radio" {{$blood->hiv_hep_std=="no"?'checked':""}} name="hiv_hep_std" value="no">
                              <label class="custom-control-label" for="customRadio1">No</label>
                          </div>
                          @error('hiv_hep_std')
                          <small class="alert-text text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="lastt_name">Have you had any of the following in the last one year?</label>
                          <div class="form-check">
                              <input class="form-check-input" type="radio" {{$blood->last_year_symptoms=="yes"?'checked':""}} name="last_year_symptoms" value="yes">
                              <label class="custom-control-label" for="customRadio1">Yes</label>
                          </div>
                          <div class="form-check">
                              <input class="form-check-input" type="radio" {{$blood->last_year_symptoms=="no"?'checked':""}} name="last_year_symptoms" value="no">
                              <label class="custom-control-label" for="customRadio1">No</label>
                          </div>
                          @error('last_year_symptoms')
                          <small class="alert-text text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Email</label>
                      <input type="email" class="form-control  @error('email') is-invalid @enderror" value="{{$blood->email}}" name="email" id="first_name" placeholder="user@example.com">
                      @error('email')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Mobile</label>
                      <input type="text" placeholder="Mobile number" value="{{$blood->mobile}}" name="mobile" class="form-control @error('mobile') is-invalid @enderror" id="last_name"/>
                      @error('mobile')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Address Line 1:</label>
                      <input type="text" class="form-control  @error('address_1') is-invalid @enderror" value="{{$blood->address_1}}" name="address_1" id="first_name" placeholder="Address Line">
                      @error('address_1')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Address Line 2:</label>
                      <input type="text" placeholder="Address Line " value="{{$blood->address_2}}" name="address_2" class="form-control @error('address_2') is-invalid @enderror" id="Address Line"/>
                      @error('address_2')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">State</label>
                      <input type="text" class="form-control @error('state') is-invalid @enderror" value="{{$blood->state}}" name="state" id="state" placeholder="State">
                      @error('state')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">District</label>
                      <input type="text" placeholder="District" value="{{$blood->district}}" name="district" class="form-control @error('district') is-invalid @enderror" id="district"/>
                      @error('district')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">City/Town/Village </label>
                      <input type="text" class="form-control @error('city') is-invalid @enderror" value="{{$blood->city}}" name="city"  id="city" placeholder="City/Town/Village">
                      @error('city')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Pincode</label>
                      <input type="text" placeholder="Pincode" value="{{$blood->pincode}}" name="pincode" class="form-control @error('pincode') is-invalid @enderror" id="pincode"/>
                      @error('pincode')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center">
                        @csrf
                        <a href="{{route('blood.index')}}" class="btn btn-outline-secondary w-10 mt-4 mb-5 px-3">Go Back</a>
                        <button type="submit" class="btn bg-gradient-info w-20 mt-4 mb-5">Submit</button>
                    </div>
                  </div>
                </div>
                {{-- <div class="row"> --}}
            </form>
          </div>
        </div>
      </div>
</div>

@endsection
