@extends('layouts.main')

@section('title', 'Permissions')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Permission</h6>
          </div>
          <div class="card-body px-4 pt-2 pb-2">
            <form action="{{route('permissions.store')}}" method="POST">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Permission</label>
                      <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" placeholder="Permission">
                      @error('name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastt_name">Guard</label>
                      {{-- <input type="text" placeholder="Your Guard Name" class="form-control @error('guard_name') is-invalid @enderror" id="guard_name" name="guard_name"/> --}}
                      <select class="form-control @error('guard_name') is-invalid @enderror" name="guard_name" id="exampleFormControlSelect1">
                        <option value="">Select a Guard</option>
                        <option>web</option>
                        <option>api</option>
                    </select>
                      @error('guard_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center">
                        @csrf
                        <a href="{{route('permissions.index')}}" class="btn btn-outline-secondary w-10 mt-4 mb-5 px-3">Go Back</a>
                        <button type="submit" class="btn bg-gradient-info w-20 mt-4 mb-5">Submit</button>
                    </div>
                  </div>
                </div>
                {{-- <div class="row"> --}}
            </form>
          </div>
        </div>
      </div>
</div>

@endsection
