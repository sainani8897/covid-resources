@extends('layouts.main')

@section('title', 'Oxygen')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="float-sm-end pb-4">
            @can('add oxygen')
            <a href="{{route('oxygen.create')}}"  class="btn btn-success">Add Oxygen <span class="btn-inner--icon"><i class="ni ni-plus-61"></i></span></a>
            @endcan
        </div>
        <div class="clearfix"></div>
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Oxygen Suppliers</h6>
          </div>
          <div class="card-body px-0 pt-0 pb-2">

            @livewire('dashboards.oxygen-donars')

          </div>
        </div>
      </div>
</div>
@endsection
