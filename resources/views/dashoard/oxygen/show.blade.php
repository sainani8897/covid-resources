@extends('layouts.main')

@section('title', 'Oxygen')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Oxygen Supplier</h6>
          </div>
          <div class="card-body px-4 pt-2 pb-2">
            <form action="{{route('oxygen.update',$oxygen->id)}}" method="POST">
                @method('PUT')
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                      <input type="text" class="form-control @error('first_name') is-invalid @enderror"  value="{{$oxygen->first_name}}" disabled name="first_name" placeholder="Your First Name">
                      @error('first_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastt_name">Last Name</label>
                      <input type="text" placeholder="Your Last Name" class="form-control @error('last_name') is-invalid @enderror" id="last_name" value="{{$oxygen->last_name}}" disabled name="last_name"/>
                      @error('last_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="company_name">Company Name</label>
                        <input type="text" class="form-control @error('company_name') is-invalid @enderror"  value="{{$oxygen->company_name}}" disabled name="company_name" placeholder="Your First Name">
                        @error('company_name')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="lastt_name">Company Website URL</label>
                        <input type="text" placeholder="Company Website URL" class="form-control @error('url') is-invalid @enderror" id="url" value="{{$oxygen->url}}" disabled name="url"/>
                        @error('url')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastt_name">Oxyzen Available</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$oxygen->availability=="yes"?"checked":''}} disabled name="availability" value="yes">
                                <label class="custom-control-label" for="customRadio1">Yes</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$oxygen->availability=="no"?"checked":'no'}} disabled name="availability" value="no">
                                <label class="custom-control-label" for="customRadio1">No</label>
                            </div>

                            @error('availability')
                            <small class="alert-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="available_units">Available Units</label>
                            <input type="number" class="form-control  @error('available_units') is-invalid @enderror" value="{{$oxygen->available_units}}" disabled name="available_units" id="available_units" placeholder="Available Units">
                            @error('available_units')
                            <small class="alert-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Email</label>
                      <input type="email" class="form-control  @error('email') is-invalid @enderror" value="{{$oxygen->email}}" disabled name="email" id="first_name" placeholder="user@example.com">
                      @error('email')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Mobile</label>
                      <input type="text" placeholder="Mobile number" value="{{$oxygen->mobile}}" disabled name="mobile" class="form-control @error('mobile') is-invalid @enderror" id="last_name"/>
                      @error('mobile')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="alt_email">Alternate Email</label>
                        <input type="email" class="form-control  @error('alt_email') is-invalid @enderror" value="{{$oxygen->alt_email}}" disabled name="alt_email" id="alt_email" placeholder="Alternate Email">
                        @error('alt_email')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="alt_mobile">Alternate Mobile</label>
                        <input type="text" placeholder="Alternate Mobile number" value="{{$oxygen->alt_mobile}}" disabled name="alt_mobile" class="form-control @error('alt_mobile') is-invalid @enderror" id="alt_mobile"/>
                        @error('alt_mobile')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Address Line 1:</label>
                      <input type="text" class="form-control  @error('address_1') is-invalid @enderror" value="{{$oxygen->address_1}}" disabled name="address_1" id="first_name" placeholder="Address Line">
                      @error('address_1')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Address Line 2:</label>
                      <input type="text" placeholder="Address Line " value="{{$oxygen->address_2}}" disabled name="address_2" class="form-control @error('address_2') is-invalid @enderror" id="Address Line"/>
                      @error('address_2')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">State</label>
                      <input type="text" class="form-control @error('state') is-invalid @enderror" value="{{$oxygen->state}}" disabled name="state" id="state" placeholder="State">
                      @error('state')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">District</label>
                      <input type="text" placeholder="District" value="{{$oxygen->district}}" disabled name="district" class="form-control @error('district') is-invalid @enderror" id="district"/>
                      @error('district')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">City/Town/Village </label>
                      <input type="text" class="form-control @error('city') is-invalid @enderror" value="{{$oxygen->city}}" disabled name="city"  id="city" placeholder="City/Town/Village">
                      @error('city')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Pincode</label>
                      <input type="text" placeholder="Pincode" value="{{$oxygen->pincode}}" disabled name="pincode" class="form-control @error('pincode') is-invalid @enderror" id="pincode"/>
                      @error('pincode')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="first_name">Latitude </label>
                        <input type="text" class="form-control @error('latitude') is-invalid @enderror" value="{{$oxygen->latitude}}" disabled name="latitude"  id="latitude" placeholder="Latitude">
                        @error('latitude')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="lastt_name">Longitude</label>
                        <input type="text" placeholder="longitude" value="{{$oxygen->longitude}}" disabled name="longitude" class="form-control @error('longitude') is-invalid @enderror" id="longitude"/>
                        @error('longitude')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center">
                        @csrf
                        <a href="{{route('oxygen.index')}}" class="btn btn-outline-secondary bg-gradient-info w-30 mt-4 mb-5">Go Back</a>
                    </div>
                  </div>
                </div>
                {{-- <div class="row"> --}}
            </form>
          </div>
        </div>
      </div>
</div>

@endsection
