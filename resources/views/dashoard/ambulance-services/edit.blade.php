@extends('layouts.main')

@section('title', 'Ambulance Services')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Oxygen Supplier</h6>
          </div>
          <div class="card-body px-4 pt-2 pb-2">
            <form action="{{route('ambulance-services.update',$ambulaceService->id)}}" method="POST">
                @method('PUT')
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                      <input type="text" class="form-control @error('first_name') is-invalid @enderror"  value="{{$ambulaceService->first_name}}" name="first_name" placeholder="Your First Name">
                      @error('first_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastt_name">Last Name</label>
                      <input type="text" placeholder="Your Last Name" class="form-control @error('last_name') is-invalid @enderror" id="last_name" value="{{$ambulaceService->last_name}}" name="last_name"/>
                      @error('last_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="service_name">Service Name</label>
                        <input type="text" class="form-control @error('service_name') is-invalid @enderror"  value="{{$ambulaceService->service_name}}" name="service_name" placeholder="Your First Name">
                        @error('service_name')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="lastt_name">Company Website URL</label>
                        <input type="text" placeholder="Company Website URL" class="form-control @error('url') is-invalid @enderror" id="url" value="{{$ambulaceService->url}}" name="url"/>
                        @error('url')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastt_name">Available</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$ambulaceService->availability=="yes"?"checked":''}} name="availability" value="yes">
                                <label class="custom-control-label" for="customRadio1">Yes</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" {{$ambulaceService->availability=="no"?"checked":'no'}} name="availability" value="no">
                                <label class="custom-control-label" for="customRadio1">No</label>
                            </div>

                            @error('availability')
                            <small class="alert-text text-danger">{{ $message }}</small>
                          @enderror
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <label for="available_units">Types of Abmulamce</label>
                              <select class="form-control js-example-basic-multiple-edit @error('types') is-invalid @enderror" name="type_id[]" multiple>
                                  <option value="">Select Types of Abmulamce Services</option>
                                  @foreach ($types as $type)
                                      <option value="{{$type->id}}">{{$type->type}}</option>
                                  @endforeach
                              </select>
                            @error('type_id')
                            <small class="alert-text text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Email</label>
                      <input type="email" class="form-control  @error('email') is-invalid @enderror" value="{{$ambulaceService->email}}" name="email" id="first_name" placeholder="user@example.com">
                      @error('email')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Mobile</label>
                      <input type="text" placeholder="Mobile number" value="{{$ambulaceService->mobile}}" name="mobile" class="form-control @error('mobile') is-invalid @enderror" id="last_name"/>
                      @error('mobile')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="alt_email">Alternate Email</label>
                        <input type="email" class="form-control  @error('alt_email') is-invalid @enderror" value="{{$ambulaceService->alt_email}}" name="alt_email" id="alt_email" placeholder="Alternate Email">
                        @error('alt_email')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="alt_mobile">Alternate Mobile</label>
                        <input type="text" placeholder="Alternate Mobile number" value="{{$ambulaceService->alt_mobile}}" name="alt_mobile" class="form-control @error('alt_mobile') is-invalid @enderror" id="alt_mobile"/>
                        @error('alt_mobile')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Address Line 1:</label>
                      <input type="text" class="form-control  @error('address_1') is-invalid @enderror" value="{{$ambulaceService->address_1}}" name="address_1" id="first_name" placeholder="Address Line">
                      @error('address_1')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Address Line 2:</label>
                      <input type="text" placeholder="Address Line " value="{{$ambulaceService->address_2}}" name="address_2" class="form-control @error('address_2') is-invalid @enderror" id="Address Line"/>
                      @error('address_2')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">State</label>
                      <input type="text" class="form-control @error('state') is-invalid @enderror" value="{{$ambulaceService->state}}" name="state" id="state" placeholder="State">
                      @error('state')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">District</label>
                      <input type="text" placeholder="District" value="{{$ambulaceService->district}}" name="district" class="form-control @error('district') is-invalid @enderror" id="district"/>
                      @error('district')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">City/Town/Village </label>
                      <input type="text" class="form-control @error('city') is-invalid @enderror" value="{{$ambulaceService->city}}" name="city"  id="city" placeholder="City/Town/Village">
                      @error('city')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Pincode</label>
                      <input type="text" placeholder="Pincode" value="{{$ambulaceService->pincode}}" name="pincode" class="form-control @error('pincode') is-invalid @enderror" id="pincode"/>
                      @error('pincode')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="first_name">Latitude </label>
                        <input type="text" class="form-control @error('latitude') is-invalid @enderror" value="{{$ambulaceService->latitude}}" name="latitude"  id="latitude" placeholder="Latitude">
                        @error('latitude')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label for="lastt_name">Longitude</label>
                        <input type="text" placeholder="longitude" value="{{$ambulaceService->longitude}}" name="longitude" class="form-control @error('longitude') is-invalid @enderror" id="longitude"/>
                        @error('longitude')
                          <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                      </div>
                    </div>
                  </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center">
                        @csrf
                        <a href="{{route('ambulance-services.index')}}" class="btn btn-outline-secondary w-10 mt-4 mb-5 px-3">Go Back</a>
                        <button type="submit" class="btn bg-gradient-info w-20 mt-4 mb-5">Submit</button>
                    </div>
                  </div>
                </div>
                {{-- <div class="row"> --}}
            </form>
          </div>
        </div>
      </div>
</div>
@php
    $sel = json_encode($ambulaceService->abmulace_service_types_ids);
    // dd($sel);
@endphp
@push('scripts')
<script>
    $(".js-example-basic-multiple-edit").val(JSON.parse(`<?php echo $sel ?>`)).trigger('change');
    $(document).ready(function() {
          $('.js-example-basic-multiple-edit').select2({
              placeholder: 'Select an option',
              theme: "classic"
          });
      });
</script>
@endpush
@endsection
