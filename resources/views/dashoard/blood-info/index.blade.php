@extends('layouts.main')

@section('title', 'Blood')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="float-sm-end pb-4">
            @can('add blood')
                <a href="{{route('blood.create')}}"  class="btn btn-success">Add Blood <span class="btn-inner--icon"><i class="ni ni-plus-61"></i></span></a>
            @endcan
        </div>
        <div class="clearfix"></div>
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Plasma Donars</h6>
          </div>
          <div class="card-body px-0 pt-0 pb-2">

            @livewire('dashboards.blood-donars-component')

          </div>
        </div>
      </div>
</div>
@endsection
