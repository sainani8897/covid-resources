@extends('layouts.main')

@section('title', 'Plasma')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card mb-4">
          <div class="card-header pb-0">
            <h6>Donor Information</h6>
          </div>
          <div class="card-body px-4 pt-2 pb-2">
            <form action="{{route('plasma.update',$plasma->id)}}" method="POST">
                @method('PUT')
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                      <input type="text" class="form-control @error('first_name') is-invalid @enderror"  value="{{$plasma->first_name}}" readonly name="first_name" placeholder="Your First Name">
                      @error('first_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lastt_name">Last Name</label>
                      <input type="text" placeholder="Your Last Name" class="form-control @error('last_name') is-invalid @enderror" id="last_name" value="{{$plasma->last_name}}" readonly name="last_name"/>
                      @error('last_name')
                      <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="example-date-input" class="form-control-label">Date of Bith (D.O.B)</label>
                        <input class="form-control @error('bith_date') is-invalid @enderror" type="date" value="{{date('Y-m-d',strtotime($plasma->bith_date))}}" readonly name="bith_date"  id="bith_date">
                        @error('bith_date')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Age</label>
                      <input type="text" placeholder="Age" class="form-control" id="age" disabled/>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Gender</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->gender=="male"?'checked':""}} disabled name="gender" value="male">
                            <label class="custom-control-label" for="customRadio1">Male</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->gender=="female"?'checked':""}} disabled name="gender" value="female">
                            <label class="custom-control-label" for="customRadio1">Female</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->gender=="other"?'checked':""}}  disabled name="gender" value="other">
                            <label class="custom-control-label" for="customRadio1">Other</label>
                        </div>
                        @error('gender')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Blood Group</label>
                        <select class="form-control @error('blood_group') is-invalid @enderror" value="{{$plasma->id}}" disabled name="blood_group" id="exampleFormControlSelect1">
                            <option value="">Select a Blood Group</option>
                            <option {{$plasma->blood_group=="O+"?'selected':""}} >O+</option>
                            <option {{$plasma->blood_group=="O-"?'selected':""}} >O-</option>
                            <option {{$plasma->blood_group=="A+"?'selected':""}} >A+</option>
                            <option {{$plasma->blood_group=="A-"?'selected':""}} >A-</option>
                            <option {{$plasma->blood_group=="B-"?'selected':""}} >B-</option>
                            <option {{$plasma->blood_group=="B+"?'selected':""}} >B+</option>
                            <option {{$plasma->blood_group=="AB+"?'selected':""}} >AB+</option>
                            <option {{$plasma->blood_group=="AB-"?'selected':""}} >AB-</option>
                        </select>
                        @error('blood_group')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Was your COVID-19 diagnosis confirmed by a lab test?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->postive_test=='yes'?"checked":""}} disabled name="postive_test" value="yes">
                            <label class="custom-control-label" for="customRadio1">Yes</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->postive_test=='no'?"checked":""}} disabled name="postive_test" value="no">
                            <label class="custom-control-label" for="customRadio1">No</label>
                        </div>
                        @error('postive_test')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Do you currently have symptoms?</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio"
                            {{$plasma->current_symptoms=='yes'?"checked":""}} disabled name="current_symptoms" value="yes">
                            <label class="custom-control-label" for="customRadio1">Yes</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio"
                            {{$plasma->current_symptoms=='no'?"checked":""}} disabled name="current_symptoms" value="no">
                            <label class="custom-control-label" for="customRadio1">No</label>
                        </div>
                        @error('current_symptoms')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Have you had a follow up test that was negative for COVID-19</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->negetive_test=='yes'?"checked":""}} disabled name="negetive_test" value="yes">
                            <label class="custom-control-label" for="customRadio1">Yes</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" {{$plasma->negetive_test=='no'?"checked":""}} disabled name="negetive_test" value="no">
                            <label class="custom-control-label" for="customRadio1">No</label>
                        </div>
                        @error('negetive_test')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Date of Recovery (Covid)</label>
                        <input class="form-control @error('recovery_date') is-invalid @enderror" type="date" value="{{date('Y-m-d',strtotime($plasma->recovery_date))}}" readonly name="recovery_date"  id="bith_date">
                        @error('recovery_date')
                        <small class="alert-text text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Email</label>
                      <input type="email" class="form-control  @error('email') is-invalid @enderror" value="{{$plasma->email}}" readonly name="email" id="first_name" placeholder="user@example.com">
                      @error('email')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Mobile</label>
                      <input type="text" placeholder="Mobile number" value="{{$plasma->mobile}}" readonly name="mobile" class="form-control @error('mobile') is-invalid @enderror" id="last_name"/>
                      @error('mobile')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">Address Line 1:</label>
                      <input type="text" class="form-control  @error('address_1') is-invalid @enderror" value="{{$plasma->address_1}}" readonly name="address_1" id="first_name" placeholder="Address Line">
                      @error('address_1')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Address Line 2:</label>
                      <input type="text" placeholder="Address Line " value="{{$plasma->address_2}}" readonly name="address_2" class="form-control @error('address_2') is-invalid @enderror" id="Address Line"/>
                      @error('address_2')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">State</label>
                      <input type="text" class="form-control @error('state') is-invalid @enderror" value="{{$plasma->state}}" readonly name="state" id="state" placeholder="State">
                      @error('state')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">District</label>
                      <input type="text" placeholder="District" value="{{$plasma->district}}" readonly name="district" class="form-control @error('district') is-invalid @enderror" id="district"/>
                      @error('district')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="first_name">City/Town/Village </label>
                      <input type="text" class="form-control @error('city') is-invalid @enderror" value="{{$plasma->city}}" readonly name="city"  id="city" placeholder="City/Town/Village">
                      @error('city')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="lastt_name">Pincode</label>
                      <input type="text" placeholder="Pincode" value="{{$plasma->pincode}}" readonly name="pincode" class="form-control @error('pincode') is-invalid @enderror" id="pincode"/>
                      @error('pincode')
                        <small class="alert-text text-danger">{{ $message }}</small>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-center">
                        @csrf
                        <a href="{{route('plasma.index')}}" class="btn  bg-gradient-info w-30 mt-4 mb-5">Go back</a>
                    </div>
                  </div>
                </div>
                {{-- <div class="row"> --}}
            </form>
          </div>
        </div>
      </div>
</div>

@endsection
