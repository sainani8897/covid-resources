<div>
    {{-- The Master doesn't talk, he acts. --}}

    <div class="col-xl-3 col-lg-6 col-md-6 mb-1 px-3">
        <input wire:model="search" class="form-control" type="text" placeholder="Search ...">
        {{-- <a href="{{route('admin.day.sales.export',['month'=>$])}}" class="btn btn-info">Export</a> --}}
    </div>
    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
          <thead>
            <tr>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                <a wire:click.prevent="sortBy('first_name')" role="button" href="#">Supplier
                @include('includes._sort-icon', ['field' => 'first_name'])</a> </th>
              </th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('company_name')" role="button" href="#">Company name
                @include('includes._sort-icon', ['field' => 'company_name'])</a></th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('availabilty')" role="button" href="#">Availabilty
                @include('includes._sort-icon', ['field' => 'availabilty'])</a></th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('available_units')" role="button" href="#">Availabe Units
                @include('includes._sort-icon', ['field' => 'available_units'])</a></th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('state')" role="button" href="#">State
                @include('includes._sort-icon', ['field' => 'state'])</a></th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('district')" role="button" href="#">District
                @include('includes._sort-icon', ['field' => 'district'])</a></th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('city')" role="button" href="#">city
                @include('includes._sort-icon', ['field' => 'city'])</a></th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                  <a wire:click.prevent="sortBy('status')" role="button" href="#">Status
                @include('includes._sort-icon', ['field' => 'status'])</a></th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                <a wire:click.prevent="sortBy('created_at')" role="button" href="#">Created At
                    @include('includes._sort-icon', ['field' => 'created_at'])
                </a>
              </th>
              <th class="text-secondary opacity-7">Action</th>
            </tr>
          </thead>
          <tbody>

              {{-- {{dd($oxygen_suppliers)}} --}}
            @foreach ($oxygen_suppliers as $oxygen)
            <tr>
                <td>
                  <div class="d-flex px-2 py-1">
                    <div class="d-flex flex-column justify-content-center">
                      <h6 class="mb-0 text-sm">{{$oxygen->first_name.' '.$oxygen->last_name}}</h6>
                      <p class="text-xs text-secondary mb-0">{{$oxygen->email}}</p>
                      <p class="text-xs text-secondary mb-0">{{$oxygen->mobile}}</p>
                    </div>
                  </div>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$oxygen->company_name}}</p>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$oxygen->availability}}</p>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$oxygen->available_units}}</p>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$oxygen->state}}</p>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$oxygen->district}}</p>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$oxygen->city}}</p>
                </td>
                <td class="align-middle text-center text-sm">
                  <span class="badge badge-sm bg-gradient-success">{{$oxygen->status}}</span>
                </td>
                <td class="align-middle text-center">
                  <span class="text-secondary text-xs font-weight-bold">{{$oxygen->created_at}}</span>
                </td>
                <td class="align-middle">
                    @can('edit oxygen')
                    <a href="{{route('oxygen.edit',$oxygen->id)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Edit">
                      <i class="fas fa-edit"></i>
                    </a>
                    @endcan
                    @can('edit oxygen')
                    <a data-bs-toggle="modal" data-bs-target="#modal-notification" class="trigger-btn" data-toggle="modal" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Delete">
                      <i class="fas fa-trash-alt"></i>
                    </a>
                    @endcan
                    @can('show oxygen')
                    <a href="{{route('oxygen.show',$oxygen->id)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="view">
                      <i class="fas fa-table"></i>
                    </a>
                    @endcan
                    @can('verify oxygen')
                    <a href="{{route('oxygen.verify',$oxygen->id,$_GET)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Edit user">
                    <i class="fas fa-check-square"></i>
                    </a>
                    @endcan
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>

        {{ $oxygen_suppliers->links() }}

      </div>


      @include('includes.delete-modal', ['variableName' => []])


</div>

