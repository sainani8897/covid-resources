<div>
    {{-- The Master doesn't talk, he acts. --}}

    <div class="col-xl-3 col-lg-6 col-md-6 mb-1 px-3">
        <input wire:model="search" class="form-control" type="text" placeholder="Search ...">
        {{-- <a href="{{route('admin.day.sales.export',['month'=>$])}}" class="btn btn-info">Export</a> --}}
    </div>
    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
          <thead>
            <tr>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                <a wire:click.prevent="sortBy('first_name')" role="button" href="#">Requestor Details
                @include('includes._sort-icon', ['field' => 'first_name'])</a> </th>
              </th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('blood_group')" role="button" href="#">Request Type
                @include('includes._sort-icon', ['field' => 'blood_group'])</a></th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                  <a wire:click.prevent="sortBy('status')" role="button" href="#">Status
                @include('includes._sort-icon', ['field' => 'status'])</a></th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                <a wire:click.prevent="sortBy('created_at')" role="button" href="#">Created At
                    @include('includes._sort-icon', ['field' => 'created_at'])
                </a>
              </th>
              <th class="text-secondary opacity-7">Action</th>
            </tr>
          </thead>
          <tbody>

              {{-- {{dd($requestData_donars)}} --}}
            @foreach ($request_forms as $requestData)
            <tr>
                <td>
                  <div class="d-flex px-2 py-1">
                    <div class="d-flex flex-column justify-content-center">
                      <h6 class="mb-0 text-sm">{{$requestData->first_name.' '.$requestData->last_name}}</h6>
                      <p class="text-xs text-secondary mb-0">{{$requestData->email}}</p>
                    </div>
                  </div>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$requestData->requests}}</p>
                  {{-- <p class="text-xs text-secondary mb-0">Organization</p> --}}
                </td>
                <td class="align-middle text-center text-sm">
                  <span class="badge badge-sm bg-gradient-success">{{$requestData->status}}</span>
                </td>
                <td class="align-middle text-center">
                  <span class="text-secondary text-xs font-weight-bold">{{$requestData->created_at}}</span>
                </td>
                <td class="align-middle">
                    {{-- @can('edit requests') --}}
                    <a href="{{route('requests.edit',$requestData->id)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Edit">
                      <i class="fas fa-edit"></i>
                    </a>
                    {{-- @endcan --}}
                    @can('edit requests')
                    <a data-bs-toggle="modal" data-bs-target="#modal-notification" class="trigger-btn" data-toggle="modal" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Delete">
                      <i class="fas fa-trash-alt"></i>
                    </a>
                    @endcan
                    @can('show requests')
                    <a href="{{route('requests.show',$requestData->id)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="view">
                      <i class="fas fa-table"></i>
                    </a>
                    @endcan
                    @can('verify requests')
                    <a href="{{route('blood.verify',$requestData->id,$_GET)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Edit user">
                        <i class="fas fa-check-square"></i>
                    </a>
                    @endcan
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>

        {{ $request_forms->links() }}

      </div>


      @include('includes.delete-modal', ['variableName' => []])


</div>
