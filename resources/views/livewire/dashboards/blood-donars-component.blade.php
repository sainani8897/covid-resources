<div>
    {{-- The Master doesn't talk, he acts. --}}

    <div class="col-xl-3 col-lg-6 col-md-6 mb-1 px-3">
        <input wire:model="search" class="form-control" type="text" placeholder="Search ...">
        {{-- <a href="{{route('admin.day.sales.export',['month'=>$])}}" class="btn btn-info">Export</a> --}}
    </div>
    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
          <thead>
            <tr>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                <a wire:click.prevent="sortBy('first_name')" role="button" href="#">Donar
                @include('includes._sort-icon', ['field' => 'first_name'])</a> </th>
              </th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                  <a wire:click.prevent="sortBy('blood_group')" role="button" href="#">Blood Group
                @include('includes._sort-icon', ['field' => 'blood_group'])</a></th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                  <a wire:click.prevent="sortBy('status')" role="button" href="#">Status
                @include('includes._sort-icon', ['field' => 'status'])</a></th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">
                <a wire:click.prevent="sortBy('created_at')" role="button" href="#">Created At
                    @include('includes._sort-icon', ['field' => 'created_at'])
                </a>
              </th>
              <th class="text-secondary opacity-7">Action</th>
            </tr>
          </thead>
          <tbody>

              {{-- {{dd($blood_donar_donars)}} --}}
            @foreach ($blood_donars as $blood_donar)
            <tr>
                <td>
                  <div class="d-flex px-2 py-1">
                    <div class="d-flex flex-column justify-content-center">
                      <h6 class="mb-0 text-sm">{{$blood_donar->first_name.' '.$blood_donar->last_name}}</h6>
                      <p class="text-xs text-secondary mb-0">{{$blood_donar->email}}</p>
                    </div>
                  </div>
                </td>
                <td>
                  <p class="text-xs font-weight-bold mb-0">{{$blood_donar->blood_group}}</p>
                  {{-- <p class="text-xs text-secondary mb-0">Organization</p> --}}
                </td>
                <td class="align-middle text-center text-sm">
                  <span class="badge badge-sm bg-gradient-success">{{$blood_donar->status}}</span>
                </td>
                <td class="align-middle text-center">
                  <span class="text-secondary text-xs font-weight-bold">{{$blood_donar->created_at}}</span>
                </td>
                <td class="align-middle">
                    @can('edit blood')
                    <a href="{{route('blood.edit',$blood_donar->id)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Edit">
                      <i class="fas fa-edit"></i>
                    </a>
                    @endcan
                    @can('edit blood')
                    <a data-bs-toggle="modal" data-bs-target="#modal-notification" class="trigger-btn" data-toggle="modal" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Delete">
                      <i class="fas fa-trash-alt"></i>
                    </a>
                    @endcan
                    @can('show blood')
                    <a href="{{route('blood.show',$blood_donar->id)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="view">
                      <i class="fas fa-table"></i>
                    </a>
                    @endcan
                    @can('verify blood')
                    <a href="{{route('blood.verify',$blood_donar->id,$_GET)}}" class="text-secondary font-weight-bold" data-toggle="tooltip" data-original-title="Edit user">
                        <i class="fas fa-check-square"></i>
                    </a>
                    @endcan
                </td>
              </tr>
            @endforeach

          </tbody>
        </table>

        {{ $blood_donars->links() }}

      </div>


      @include('includes.delete-modal', ['variableName' => []])


</div>
