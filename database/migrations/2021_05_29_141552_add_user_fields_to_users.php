<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->date('birth_date')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender', 255)->nullable();
            $table->string('blood_group', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('mobile', 255)->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('district', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->integer('pincode', 255)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('birth_date');
            $table->dropColumn('age');
            $table->dropColumn('gender');
            $table->dropColumn('blood_group');
            $table->dropColumn('email');
            $table->dropColumn('mobile');
            $table->dropColumn('address_1');
            $table->dropColumn('address_1');
            $table->dropColumn('state');
            $table->dropColumn('district');
            $table->dropColumn('city');
             $table->dropSoftDeletes();
        });
    }
}
