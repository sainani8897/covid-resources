<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloodDonarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blood_donars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->date('birth_date')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender', 255)->nullable();
            $table->string('blood_group', 255)->nullable();
            $table->string('blood_donation_histroy', 255)->nullable();
            $table->string('current_symptoms', 255)->nullable();
            $table->string('hiv_hep_std', 255)->nullable();
            $table->string('last_year_symptoms', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('mobile', 255)->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('district', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->integer('pincode', 255)->nullable();
            $table->enum('status',['verified','un-verified','not-reachable','not-lifiting','ringing','not-responding','available'])->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blood_donars');
    }
}
