<?php

use App\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = User::create([
            "name"=>"super admin",
            "email"=>"admin@covidresource.com",
            "password"=>bcrypt('Admin@123'),
        ]);

        $user->assignRole('super-admin');
    }
}
