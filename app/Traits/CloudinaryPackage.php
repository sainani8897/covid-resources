<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;

trait CloudinaryPackage
{
    /**
    *  Using the Cloudinary Facade
    */

    // Upload an Image File to Cloudinary with One line of Code
    public function imageUpload($file) : string
    {
        return Cloudinary::upload($file->getRealPath())->getSecurePath();
    }

    // Upload a Video File to Cloudinary with One line of Code
    public function videoUpload($request) :string
    {
        return Cloudinary::uploadVideo($request->file('file')->getRealPath())->getSecurePath();
    }

    // Upload any File to Cloudinary with One line of Code
    public function allFileUpload($request) : string
    {
        return Cloudinary::uploadFile($request->file('file')->getRealPath())->getSecurePath();
    }

    /**
     *  This package also exposes a helper function you can use if you are not a fan of Facades
     *  Shorter, expressive, fluent using the
     *  cloudinary() function
     */

    // Upload an image file to cloudinary with one line of code
    public function uploadImageHelper($request)
    {
        return cloudinary()->upload($request->file('file')->getRealPath())->getSecurePath();
    }

    // Upload a video file to cloudinary with one line of code
    public function uploadVideoHelper($request)
    {
        return cloudinary()->uploadVideo($request->file('file')->getRealPath())->getSecurePath();
    }

    // Upload any file  to cloudinary with one line of code
    public function uploadAllFilesHelper($request)
    {
        return cloudinary()->uploadFile($request->file('file')->getRealPath())->getSecurePath();
    }

    // Upload an existing remote file to Cloudinary with one line of code
    public function remoteFileUpload(Type $var = null)
    {
        return cloudinary()->uploadFile($remoteFileUrl)->getSecurePath();
    }

    /**
     *  You can also skip the Cloudinary Facade or helper method and laravel-ize your uploads by simply calling the
     *  storeOnCloudinary() method on the file itself
     */

    // Store the uploaded file on Cloudinary    $result = $request->file('file')->storeOnCloudinary();

    // Store the uploaded file on Cloudinary
    // $result = $request->file->storeOnCloudinary();

    // // Store the uploaded file in the "lambogini" directory on Cloudinary
    // $result = $request->file->storeOnCloudinary('lambogini');

    // // Store the uploaded file in the "lambogini" directory on Cloudinary with the filename "prosper"
    // $result = $request->file->storeOnCloudinaryAs('lambogini', 'prosper');

    // $result->getPath(); // Get the url of the uploaded file; http
    // $result->getSecurePath(); // Get the url of the uploaded file; https
    // $result->getSize(); // Get the size of the uploaded file in bytes
    // $result->getReadableSize(); // Get the size of the uploaded file in bytes, megabytes, gigabytes or terabytes. E.g 1.8 MB
    // $result->getFileType(); // Get the type of the uploaded file
    // $result->getFileName(); // Get the file name of the uploaded file
    // $result->getOriginalFileName(); // Get the file name of the file before it was uploaded to Cloudinary
    // $result->getPublicId(); // Get the public_id of the uploaded file
    // $result->getExtension(); // Get the extension of the uploaded file
    // $result->getWidth(); // Get the width of the uploaded file
    // $result->getHeight(); // Get the height of the uploaded file
    // $result->getTimeUploaded(); // Get the time the file was uploaded

    /**
     * You can also retrieve a url if you have a public id
     */

    // $url = Storage::disk('cloudinary')->url($publicId);


}

