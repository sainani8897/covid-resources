<?php

namespace App\Http\Controllers\Admin;

use App\Oxygen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOxygenRequest;

class AddOxygenController extends Controller
{
    protected $plasma;
    protected $notification;

    public function __construct()
    {
        $this->notification = array(
            'message' => 'created successfully!',
            'alert-type' => 'success'
        );
        $this->middleware(['permission:add oxygen|edit oxygen|show oxygen|verify oxygenn'])->except(['index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashoard.oxygen.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('dashoard.oxygen.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOxygenRequest $request)
    {
        $oxygen = Oxygen::create($request->all());
        return  redirect()->route('oxygen.index')->with($this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Oxygen $oxygen)
    {
        return view('dashoard.oxygen.show',compact('oxygen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Oxygen $oxygen)
    {
        return view('dashoard.oxygen.edit',compact('oxygen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreOxygenRequest $request, Oxygen $oxygen)
    {
        $oxygen = $oxygen->update($request->all());
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('oxygen.index')->with($this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function verify(Request $request,Oxygen $oxygen)
    {
        $status  = $request->verify;
        $oxygen->update(['status'=>'verified']);
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('oxygen.index',['page' => $request->get('page', 1)])->with($this->notification);
    }
}
