<?php

namespace App\Http\Controllers\Admin;

use App\PlasmaDonars;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePlasmaDonarsRequest;

class AddPlasmaDonerController extends Controller
{
    protected $plasma;
    protected $notification;

    public function __construct()
    {
        $this->notification = array(
            'message' => 'created successfully!',
            'alert-type' => 'success'
        );
        $this->middleware(['permission:add plasma|edit plasma|show plasma|verify plasma'])->except(['index']);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashoard.plasma-info.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashoard.plasma-info.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlasmaDonarsRequest $request)
    {
        $plasmaDonar = PlasmaDonars::create($request->all());
        return  redirect()->route('plasma.index')->with($this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PlasmaDonars $plasma)
    {
        return view('dashoard.plasma-info.show',compact('plasma'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PlasmaDonars $plasma)
    {
        return view('dashoard.plasma-info.edit',compact('plasma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePlasmaDonarsRequest $request, PlasmaDonars $plasma)
    {
        $plasmaDonar = $plasma->update($request->all());
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('plasma.index')->with($this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify(Request $request,PlasmaDonars $plasma)
    {
        $status  = $request->verify;
        $plasma->update(['status'=>'verified']);
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('plasma.index',['page' => $request->get('page', 1)])->with($this->notification);
    }
}
