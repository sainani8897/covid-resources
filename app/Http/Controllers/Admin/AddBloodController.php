<?php

namespace App\Http\Controllers\Admin;

use App\BloodDonars;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBloodDonarsRequest;

class AddBloodController extends Controller
{
    protected $plasma;
    protected $notification;

    public function __construct()
    {
        $this->notification = array(
            'message' => 'created successfully!',
            'alert-type' => 'success'
        );

        $this->middleware(['permission:add blood|edit blood|show blood|verify blood','verified'])->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashoard.blood-info.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashoard.blood-info.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBloodDonarsRequest $request)
    {
        $bloodDonar = BloodDonars::create($request->all());
        return  redirect()->route('blood.index')->with($this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BloodDonars $blood)
    {
        return view('dashoard.blood-info.show',compact('blood'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BloodDonars $blood)
    {
        return view('dashoard.blood-info.edit',compact('blood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBloodDonarsRequest $request, BloodDonars $blood)
    {
        $bloodDonar = $blood->update($request->all());
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('blood.index')->with($this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verify(Request $request, BloodDonars $bloodDonar)
    {
        $status  = $request->verify;
        $bloodDonar->update(['status'=>'verified']);
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('blood.index',['page' => $request->get('page', 1)])->with($this->notification);
    }
}
