<?php

namespace App\Http\Controllers\Admin;

use App\Request as FormRequests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequests;

class RequestsController extends Controller
{
    protected $plasma;
    protected $notification;

    public function __construct()
    {
        $this->notification = array(
            'message' => 'created successfully!',
            'alert-type' => 'success'
        );

        // $this->middleware(['permission:add blood|edit blood|show blood|verify blood','verified'])->except(['index','add']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashoard.requests.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashoard.requests.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequests $request)
    {
        $request = FormRequests::create($request->all());
        return  redirect()->route('blood.index')->with($this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FormRequests $request)
    {
         return view('dashoard.requests.edit',compact('request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request = FormRequests::create($request->all());
        return  redirect()->route('blood.index')->with($this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
