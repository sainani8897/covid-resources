<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Traits\CloudinaryPackage;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;

class UserController extends Controller
{
    use CloudinaryPackage;

    protected $notification;

    public function __construct()
    {
        $this->notification = array(
            'message' => 'created successfully!',
            'alert-type' => 'success'
        );
    }

    public function profile()
    {
        $user = auth()->user();
        return view('dashoard.profile.profile',compact('user'));
    }

    public function editProfile()
    {
        $user = auth()->user();
        return view('dashoard.profile.edit',compact('user'));
    }

    public function update(StoreUserRequest $request)
    {
        $user = auth()->user();
        if($request->hasFile('image')){
            $image =$this->imageUpload($request->file('image'));
            $request->request->image = $image;
        }else{
           $request->request->image = $user->image;
        }
        $user->update($request->all());
        return  redirect()->route('profile')->with($this->notification);
    }
}
