<?php

namespace App\Http\Controllers\Admin;

use App\AbmulaceServices;
use Illuminate\Http\Request;
use App\AbmulaceServiceTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAmbulanceServicesRequest;

class AddAmbulanceServicesController extends Controller
{
    protected $plasma;
    protected $notification;

    public function __construct()
    {
        $this->notification = array(
            'message' => 'created successfully!',
            'alert-type' => 'success'
        );
        $this->middleware(['permission:add ambulance|edit ambulance|show ambulance|verify ambulance'])->except(['index']);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashoard.ambulance-services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = AbmulaceServiceTypes::get();
        return view('dashoard.ambulance-services.add',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAmbulanceServicesRequest $request)
    {
        $ambmulaceService = AbmulaceServices::create($request->all());
        $ambmulaceTypes  =  AbmulaceServiceTypes::whereIn('_id',$request->type_id)->get();
        $res  =  $ambmulaceTypes->each(function ($item, $key){
            $ambmulaceService->serviceTypes()->save($item);
        });
        return  redirect()->route('ambulance-services.index')->with($this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(AbmulaceServices $ambulance_service)
    {
        $ambulaceService = $ambulance_service;
        $types = AbmulaceServiceTypes::get();
        return view('dashoard.ambulance-services.show',compact('ambulaceService','types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AbmulaceServices $ambulance_service)
    {
        $ambulaceService = $ambulance_service;
        $types = AbmulaceServiceTypes::get();
        return view('dashoard.ambulance-services.edit',compact('ambulaceService','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAmbulanceServicesRequest $request, AbmulaceServices $ambulance_service)
    {
        $ambulaceService = $ambulance_service;
        $res = $ambulaceService->update($request->all());
        $ambmulaceTypes  =  AbmulaceServiceTypes::whereIn('_id',$request->type_id)->get();
        $ambulaceService->serviceTypes()->sync($request->type_id);
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('ambulance-services.index')->with($this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function verify(Request $request,AbmulaceServices $ambulance_service)
    {
        $status  = $request->verify;
        $ambulance_service->update(['status'=>'verified']);
        $this->notification['message'] = 'Updated Successfully';
        return  redirect()->route('ambulance-services.index',['page' => $request->get('page', 1)])->with($this->notification);
    }
}
