<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' =>  'required|max:255',
            'bith_date' =>  'required|date|max:255',
            'gender' => 'required|max:255',
            // 'blood_group' => 'required|max:255',
            'email' => 'required|unique:users|max:255',
            'mobile' => 'required|unique:users|max:255',
            'pincode' => 'max:6',
        ];
    }
}
