<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' =>  'required|max:255',
            'bith_date' =>  'required|date|max:255',
            'gender' => 'required|max:255',
            'email' => 'required|email|max:255',
            'mobile' => 'required|max:255',
            'address_1' => 'required|max:255',
            'address_2' => 'required|max:255',
            'state' => 'required|max:255',
            'district' => 'required|max:255',
            'city' => 'required|max:255',
            'pincode' => 'required||max:6',
        ];
    }
}
