<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOxygenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        // $regex = "/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/";
        return [
            'first_name' => 'required|max:255',
            'last_name' =>  'required|max:255',
            'email' => 'required|email|max:255',
            'company_name' => 'required|max:255',
            'availability' => 'required|max:255',
            // 'url' => 'regex:' . $regex,
            'mobile' => 'required|max:255',
            'address_1' => 'required|max:255',
            'address_2' => 'required|max:255',
            'state' => 'required|max:255',
            'district' => 'required|max:255',
            'city' => 'required|max:255',
            'pincode' => 'required||max:6',
        ];
    }
}
