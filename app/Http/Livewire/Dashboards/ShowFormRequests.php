<?php

namespace App\Http\Livewire\Dashboards;

use App\Request as FormRequest;
use Livewire\Component;
use Livewire\WithPagination;

class ShowFormRequests extends Component
{
    use WithPagination;

    public $month = '';
    protected $sales;
    public $perPage = 10;
    public $sortField;
    public $sortAsc = false;
    public $search = null;


    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $search = $this->search;
        $sortField = $this->sortField?$this->sortField:'id';
        return view('livewire.dashboards.show-form-requests', [
            'request_forms' => FormRequest::when($search, function ($query) use ($search) {
                return $query->where(function ($query2) use ($search) {
                        $query2->where('fitst_name', 'LIKE', "%$search%")
                            ->orWhere('last_name', 'LIKE', "%$search%")
                            ->orWhere('email', 'LIKE', "%$search%")
                            ->orWhere('state', 'LIKE', "%$search%")
                            ->orWhere('district', 'LIKE', "%$search%")
                            ->orWhere('city', 'LIKE', "%$search%")
                            ->orWhere('state', 'LIKE', "%$search%")
                            ->orWhere('pincode', 'LIKE', "%$search%")
                            ->orWhere('requests', 'LIKE', "%$search%")
                            ->orWhere('created_at', 'LIKE', "%$search%");
                });
            })
            ->orderBy($sortField, $this->sortAsc ? 'asc' : 'desc')
            ->paginate(10),
        ]);
    }

}
