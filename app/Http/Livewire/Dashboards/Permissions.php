<?php

namespace App\Http\Livewire\Dashboards;

use Livewire\Component;
use Livewire\WithPagination;
use Maklad\Permission\Models\Permission;

class Permissions extends Component
{
    use WithPagination;

    public $month = '';
    protected $sales;
    public $perPage = 10;
    public $sortField;
    public $sortAsc = false;
    public $search = null;

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = ! $this->sortAsc;
        } else {
            $this->sortAsc = true;
        }

        $this->sortField = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $search = $this->search;
        $sortField = $this->sortField?$this->sortField:'id';
        return view('livewire.dashboards.permissions', [
            'permissions' => Permission::when($search, function ($query) use ($search) {
                return $query->where(function ($query2) use ($search) {
                        $query2->where('name', 'LIKE', "%$search%")
                            ->orWhere('guard_name', 'LIKE', "%$search%");
                });
            })
            ->orderBy($sortField, $this->sortAsc ? 'asc' : 'desc')
            ->paginate(10),
        ]);
    }


}
