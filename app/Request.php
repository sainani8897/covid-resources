<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Maklad\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Request extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $guarded = [];


}
