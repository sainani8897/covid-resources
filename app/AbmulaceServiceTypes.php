<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Maklad\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class AbmulaceServiceTypes extends Eloquent
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $connection = 'mongodb';


    protected $guarded = [];

    public function services()
    {
        return $this->belongsTo(AbmulaceServices::class, null, 'type_id');
    }

}
