<?php

namespace App;

use App\AbmulaceServiceTypes;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Maklad\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\BelongsToMany;

class AbmulaceServices extends Model
{
    use SoftDeletes;
    // protected $connection = 'mongodb';
    // protected $collection = 'abmulace_services';

    protected $dates = ['deleted_at'];
    protected $guarded = ['id'];


    public function serviceTypes()
    {
        return $this->belongsToMany(AbmulaceServiceTypes::class);
    }

}
